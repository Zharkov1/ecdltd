# Supplier Product List Processor

## Run project
```bash
go to /project folder
docker-compose up -d
```

## Examples

###Attributes
```bash
--source     - from which file (required attribute)
--output     - ready file (required attribute)
--separator  - separator
```

###Full example
```bash
php index.php --source=1.csv --output=2.csv --separator=,


```