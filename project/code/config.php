<?php

return [
    'headers' => [
        'make','model','colour','capacity','network','grade','condition','count'
    ],
    'csv_source_mapping' => [
        'make' => 0,
        'model' => 1,
        'colour' => 5,
        'capacity' => null,
        'network' => 6,
        'grade' => 3,
        'condition' => 2,
        'count' => null
    ]
];