<?php


class CoreController
{

    /**
     * @var mixed
     */
    private $config;

    public function __construct()
    {
        $this->config = require 'config.php';
    }

    /**
     * @var string|null
     */
    private $source;
    /**
     * @var string|null
     */
    private $output;
    /**
     * @var string|null
     */
    private $separator;

    /**
     * @throws Exception
     */
    public function dispatch(): void
    {
        $this->parseArguments();
        $this->runConvert();
    }

    private function parseArguments(): void
    {
        $this->separator = $this->separator ?? ',';
        $argv = $_SERVER['argv'];
        foreach ($argv as $item) {
            if (strpos($item, '--source') !== false) {
                $this->source = $this->setArgument($item);
            }
            if (strpos($item, '--output') !== false) {
                $this->output = $this->setArgument($item);
            }
            if (strpos($item, '--separator') !== false) {
                $this->separator = $this->setArgument($item);
            }
        }
    }

    /**
     * @param  string  $item
     * @return string|null
     */
    private function setArgument(string $item): ?string
    {
        $array = explode('=', $item);
        return array_pop($array);
    }


    /**
     * @throws Exception
     */
    private function runConvert(): void
    {
        if (empty($this->source)) {
            throw new Exception('Source argument is not valid');
        }
        if (empty($this->output)) {
            throw new Exception('Output argument is not valid');
        }
        $source_extension = pathinfo($this->source, PATHINFO_EXTENSION);
        if ($source_extension === 'csv') {
             $this->parseCsv();
        }
        if ($source_extension === 'xml') {
            //todo:parser xml
        }
        if ($source_extension === 'json') {
           //todo:parser json
        }

    }

    /**
     * @throws Exception
     */
    private function parseCsv(): void
    {
        file_put_contents("outputs/" . $this->output , '');
        if (($handle = fopen("sources/" . $this->source, 'rb')) !== FALSE) {
            while (($data = fgetcsv($handle, 10000, $this->separator)) !== FALSE) {
                $this->safeCsvString($data);
            }
            fclose($handle);
        }
    }

    /**
     * @throws Exception
     */
    public function safeCsvString(array $data): void
    {
        $output_data = [];
        foreach ($this->config['csv_source_mapping'] as $name => $index){
            if ($index !== null){
                $output_data[$name] = $data[$index] ?? '-';
            }elseif(in_array($name, ['make', 'model'])){
                throw new Exception('Source value is not found');
            }
        }
        file_put_contents("outputs/" . $this->output, implode($this->separator, $output_data) . "\n", FILE_APPEND);
    }
}